import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {RadiofusoraComponent} from './radiofusora.component';
export const routes: Routes = [

{
    path:'',
    component:RadiofusoraComponent,
    data:{
        title:'Radiofusora'
    },
}

];

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class RadiofusoraRoutingModule{}