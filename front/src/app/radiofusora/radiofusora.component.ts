import { Component, OnInit } from '@angular/core'
import { Radiofusoraservice } from '../servicios/radiofusora.service'
import { NgxSpinnerService } from "ngx-spinner"
import swal from 'sweetalert2'
import * as _ from 'lodash'
@Component({
  selector: 'app-radiofusora',
  templateUrl: './radiofusora.component.html',
  styleUrls: ['./style.css']
})
export class RadiofusoraComponent implements OnInit {
  form: any = {}
  radiofusoras: any = []
  marcas: any = []
  page = 1
  showTable = false

  constructor(public radiofusoraService: Radiofusoraservice, private spinner: NgxSpinnerService) { }

  async ngOnInit() {
    await this.getRadiofusoras(this.page)
  }

  async getRadiofusoras(page) {
    this.spinner.show();
    this.form = {}
    this.radiofusoras = await this.radiofusoraService.primeraRadiofusora({ page: page })
    this.showTable = true
    this.spinner.hide();
  }

  async anterior() {
    if (this.page > 1) {
      this.page = this.page - 1
      await this.getRadiofusoras(this.page)
    }
  }

  async siguiente() {
    this.page = this.page + 1
    await this.getRadiofusoras(this.page)
  }

  async buscarRadiofusora() {
    if (!this.form.busquedaRadiofusora) {
      await this.getRadiofusoras(1)
    }
    else {
      this.spinner.show();
      let busqueda = await this.radiofusoraService.buscarRadiofusora({ radiofusora: this.form.busquedaRadiofusora })
      if (this.radiofusoras.error) {
        swal.fire(
          '',
          this.radiofusoras.error,
          'error'
        )
        this.radiofusoras = []
      }
      else {
        this.radiofusoras = busqueda
        this.showTable = true
        console.log(this.radiofusoras);
      }
      this.spinner.hide();
    }
  }


}
