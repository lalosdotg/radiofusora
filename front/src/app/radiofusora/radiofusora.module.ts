import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadiofusoraComponent } from './radiofusora.component';
import { RadiofusoraRoutingModule } from './radiofusora-routing.module';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { DataTableModule } from 'ng-angular8-datatable';
import { NgxSpinnerModule } from "ngx-spinner";
@NgModule({
  imports: [
    CommonModule,
    RadiofusoraRoutingModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxSpinnerModule,
    DataTableModule
  ],
  declarations: [RadiofusoraComponent]
})
export class RadiofusoraModule { }
