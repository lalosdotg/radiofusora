import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {habilidadComponent} from './habilidad.component';
export const routes: Routes = [

{
    path:'',
    component:habilidadComponent,
    data:{
        title:'Habilidad'
    },
}

];

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class habilidadRoutingModule{}