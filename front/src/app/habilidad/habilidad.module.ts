import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { habilidadComponent } from './habilidad.component';
import { habilidadRoutingModule } from './habilidad-routing.module';
import { DataTableModule } from 'ng-angular8-datatable';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    habilidadRoutingModule,
    DataTableModule,
  ],
  declarations: [habilidadComponent]
})
export class habilidadModule { }
