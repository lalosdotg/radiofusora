import { Component, OnInit } from '@angular/core';
import { empleadoService } from '../servicios/empleado.service';
import * as _ from 'lodash'
import swal from 'sweetalert2'
@Component({
  selector: 'app-habilidad',
  templateUrl: './habilidad.component.html',
})
export class habilidadComponent implements OnInit {
  habilidades: any = []
  form: any = {};
  validador: any = {}
  constructor(private proveedorService: empleadoService) {
  }

  ngOnInit() {
    this.getData()
  }

  getData() {
    this.form = {}
    this.proveedorService.verhabilidades()
      .then((habilidades) => {
        this.habilidades = habilidades;
      })
  }

  guardar() {
    this.proveedorService.actualizarhabilidad(this.form)
      .then((habilidades) => {
        this.habilidades = habilidades;
        this.validador.actualizar = false
        swal.fire(
          'La posición se actualizo con exito',
        )
        this.getData()
      })
  }

  verhabilidad(habilidad) {
    this.validador.actualizar = true
    this.form = _.clone(habilidad);
  }


  agregarhabilidad() {
    this.proveedorService.registrarhabilidad(this.form)
      .then((result: any) => {
        if (result.msg) {
          this.habilidades = result.habilidades;
          this.form = {}
          swal.fire(
            'La posición se registro con exito',
          )
        } else {
          swal.fire(
            '',
            'La posición ya existe',
            'error'
          )
        }
      })
  }

  eliminar(habilidad) {
    swal.fire({
      title: '¿Estás seguro/a de eliminar la posición?',
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.value) {
        this.proveedorService.eliminarhabilidad(habilidad)
          .then((habilidades) => {
            this.getData()
            swal.fire(
              'Eliminación con éxito!',
              'success'
            )
          })

      }
    })
  }

}

