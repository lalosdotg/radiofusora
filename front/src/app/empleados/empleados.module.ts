import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {empleadoComponent} from './empleado.component';
import {empleadosRoutingModule} from './empleados-routing.module';
import { DataTableModule } from 'ng-angular8-datatable';

@NgModule({
  imports: [
    CommonModule,
    empleadosRoutingModule,
    FormsModule,
    DataTableModule
  ],
  declarations: [empleadoComponent]
})
export class empleadosModule { }
