import { Component, OnInit } from '@angular/core';
import { empleadoService } from '../servicios/empleado.service';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
})
export class empleadoComponent implements OnInit {
  form: any = {
  }
  piezas: any = []
  empleados: any = [];
  empleado: any = {}
  showBoton = false;
  habilidades: any = []

  constructor(private route: ActivatedRoute, public proveedorService: empleadoService) {

  }

  async ngOnInit() {
    await this.getData()
  }

  async getData() {
    this.form = {}
    this.showBoton = false
    this.empleados = await this.proveedorService.verempleados()
    this.habilidades = await this.proveedorService.verhabilidades()
  }

  async guardarempleado() {
    let validacion = this.validacionEmpleado()
    if (validacion) {
      let empleados = await this.proveedorService.registrarempleado(this.form)
      if (empleados) {
        this.getData()
        swal.fire({
          title: 'Personal guardado con éxito',
          type: 'success'
        })
      }
    }
  }

  validacionEmpleado() {
    if (!this.form.domicilio || !this.form.email || !this.form.fecha || !this.form.nombre || !this.form.puesto) {
      swal.fire({
        title: 'Llene todos los datos para continuar',
        type: 'warning'
      })
      return false
    }
    else return true
  }

  verempleado(empleado) {
    this.proveedorService.verempleado(empleado)
      .then((proveedoor) => {
        this.showBoton = true;
        this.form = proveedoor;
      })
  }

  actualizarempleado() {
    this.proveedorService.actualizar(this.form)
      .then(() => {
        this.getData()
        swal.fire(
          '',
          'Personal actualizado con éxito',
          'success'
        )
      })
  }
}
