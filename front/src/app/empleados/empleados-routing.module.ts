import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {empleadoComponent} from './empleado.component';
export const routes: Routes = [

{
    path:'',
    component:empleadoComponent,
    data:{
        title:'empleados'
    },
}

];

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class empleadosRoutingModule{}