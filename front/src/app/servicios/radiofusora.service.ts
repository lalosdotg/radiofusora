import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import CONFIG from '../config';
import sendToken from '../auth.interceptor'

@Injectable()
export class Radiofusoraservice {

  constructor(public http: Http) { }

  buscarRadiofusora(data) {
    return new Promise((resolve, reject) => {
      this.http.post(CONFIG.API + 'radiofusora' , data, sendToken())
        .subscribe((radiofusoras) => {
          resolve(radiofusoras.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  primeraRadiofusora(data) {
    return new Promise((resolve, reject) => {
      this.http.post(CONFIG.API + 'radiofusora/primeraRadiofusora', data, sendToken())
        .subscribe((radiofusoras) => {
          resolve(radiofusoras.json())
        }, (err) => {
          reject(err)
        })
    })
  }

}
