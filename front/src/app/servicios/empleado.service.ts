import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import CONFIG from '../config';
import sendToken from '../auth.interceptor';
@Injectable()
export class empleadoService {

  constructor(public http: Http) { }

  registrarempleado(datos) {
    return new Promise((resolve, reject) => {
      this.http.post(CONFIG.API + 'empleado', datos, sendToken())
        .subscribe((empleado) => {
          resolve(empleado.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  eliminarempleado(datos) {
    return new Promise((resolve, reject) => {
      this.http.post(CONFIG.API + 'empleado/eliminar', { _id: datos }, sendToken())
        .subscribe((empleado) => {
          resolve(empleado.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  verempleados() {
    return new Promise((resolve, reject) => {
      this.http.get(CONFIG.API + 'empleado', sendToken())
        .subscribe((empleados) => {
          resolve(empleados.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  registrarhabilidad(datos) {
    return new Promise((resolve, reject) => {
      this.http.post(CONFIG.API + 'habilidad', datos, sendToken())
        .subscribe((habilidades) => {
          resolve(habilidades.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  verhabilidades() {
    return new Promise((resolve, reject) => {
      this.http.get(CONFIG.API + 'habilidad', sendToken())
        .subscribe((habilidades) => {
          resolve(habilidades.json())
        }, (err) => {
          reject(err)
        })
    })
  }
  verNombrehabilidades() {
    return new Promise((resolve, reject) => {
      this.http.get(CONFIG.API + 'habilidad/nombres', sendToken())
        .subscribe((habilidades) => {
          resolve(habilidades.json())
        }, (err) => {
          reject(err)
        })
    })
  }


  verempleado(idempleado) {
    return new Promise((resolve, reject) => {
      this.http.get(CONFIG.API + 'empleado/' + idempleado, sendToken())
        .subscribe((proveedoor) => {
          resolve(proveedoor.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  verempleadosXhabilidad(habilidad) {
    return new Promise((resolve, reject) => {
      this.http.get(CONFIG.API + 'empleado/habilidad/' + habilidad, sendToken())
        .subscribe((proveedoores) => {
          resolve(proveedoores.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  actualizar(empleado) {
    return new Promise((resolve, reject) => {
      this.http.put(CONFIG.API + 'empleado/' + empleado._id, empleado, sendToken())
        .subscribe((empleado) => {
          resolve(empleado.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  actualizarhabilidad(habilidad) {
    return new Promise((resolve, reject) => {
      this.http.put(CONFIG.API + 'habilidad/' + habilidad._id, habilidad, sendToken())
        .subscribe((habilidades) => {
          resolve(habilidades.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  eliminarhabilidad(habilidad) {
    return new Promise((resolve, reject) => {
      this.http.delete(CONFIG.API + 'habilidad/' + habilidad._id, sendToken())
        .subscribe((habilidades) => {
          resolve(habilidades.json())
        }, (err) => {
          reject(err)
        })
    })
  }

  obtenerNombres() {
    return new Promise((resolve, reject) => {
      this.http.get(CONFIG.API + 'empleado/obtener/nombres', sendToken())
        .subscribe((nombres) => {
          resolve(nombres.json())
        }, (err) => {
          reject(err)
        })
    })
  }


}
