import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutComponent } from './layouts/full-layout.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'radiofusora',
        pathMatch: 'full'

    },
    {
        path: '',
        component: FullLayoutComponent,
       // canActivate: [AuthGuardService],
        data: {
            title: 'Inicio'
        },
        children: [
            {
                path: 'radiofusora',
                loadChildren: './radiofusora/radiofusora.module#RadiofusoraModule'
            },
            {
                path: 'habilidad',
                loadChildren: './habilidad/habilidad.module#habilidadModule'
            },
            {
                path: 'empleados',
                loadChildren: './empleados/empleados.module#empleadosModule'
            },
        ]

    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }