let dos = 2
module.exports = {
    root: true,
    env: {
      node: true,
      jest: true,
    },
    extends: "eslint:recommended",
    parserOptions: {
        "ecmaVersion": 11,
        "sourceType": "module"
    },
    rules: {
        'no-var': 1,
        'max-len': [1, { 'code': 80 }],
        'no-whitespace-before-property': [dos],
        'no-trailing-spaces': [dos],
        'object-curly-spacing': [dos, 'always'],
        'space-in-parens': [dos, 'never'],
        'space-before-function-paren': [dos, {
          'anonymous': 'always',
          'named': 'never',
          'asyncArrow': 'always'
        }],
        'no-confusing-arrow': [dos],
        'prefer-arrow-callback': [dos],
        'keyword-spacing': [dos],
        'no-multi-spaces': [dos],
        'no-await-in-loop': [dos],
        'function-paren-newline': [dos, 'multiline-arguments'],
        'no-console': [1],
        'no-magic-numbers': [1],
        'vars-on-top': [1]
    }
};