var controller=require('./habilidad.controller');
var router=require('express').Router();

router.route('/')
.post(controller.agregar)
.get(controller.verhabilidades)
router.get('/nombres',controller.verNombres)

router.put('/:id',controller.modificar)
router.delete('/:id',controller.eliminar)

module.exports=router;