var Habilidad = require('./habilidad.model');

exports.agregar = (req, res) => {
    Habilidad.findOne({
            nombre: req.body.nombre.toUpperCase()
        })
        .then((habilidad) => {
            if (!habilidad) {
                Habilidad.create(req.body)
                    .then(() => {
                        Habilidad.find()
                            .sort('nombre')
                            .then((habilidades) => {
                                res.json({
                                    habilidades: habilidades,
                                    msg: true
                                })
                            })
                    })
            } else {
                res.json({
                    msg: false
                })
            }
        })
}

exports.verhabilidades = (req, res) => {
    Habilidad.find()
        .sort('nombre')
        .then((habilidades) => {
            res.json(habilidades)
        })
}


exports.modificar = (req, res) => {
    Habilidad.findByIdAndUpdate(req.body._id, req.body)
        .then((habilidades) => {
            Habilidad.find()
                .sort('nombre')
                .then((habilidades) => {
                    res.json(habilidades)
                })
        })
}

exports.verNombres = (req, res) => {
    Habilidad.find().distinct('nombre')
        .then((habilidades) => {
            res.json(habilidades)
        })
}

exports.eliminar = (req, res) => {
    idhabilidadDesconocida = '';
    Habilidad.findOne({
            nombre: 'DESCONOCIDA'
        })
        .then((habilidad) => {
            if (habilidad) {
                idhabilidadDesconocida = habilidad._id;
            } else {
                var newhabilidad = Habilidad({
                    nombre: 'DESCONOCIDA'
                })
                return habilidad.rssave()

            }
        })
        .then((habilidad) => {
            idhabilidadDesconocida = habilidad._id;
        })
}