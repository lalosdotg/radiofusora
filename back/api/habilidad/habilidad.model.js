var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var habilidad_schema = new Schema({
    nombre: { type: String, uppercase: true },
    sueldo: Number,
    empleados: [{ type: Schema.Types.ObjectId, ref: 'empleado' }]
})

var Habilidad = mongoose.model('Habilidad', habilidad_schema);
module.exports = Habilidad