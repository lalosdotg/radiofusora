let mongoose = require('mongoose');
var Schema = mongoose.Schema;

var radiofusora_schema = new Schema({
    RADIOBASE: {
        type: String,
        uppercase: true,
        default: ""
    },
    TRAFICO: {
        type: String,
        default: ""
    },
    FECHA: {
        type: String,
        default: ""
    },
    REGION: {
        type: String,
        default: ""
    },

});

var Radiofusora = mongoose.model('Radiofusora', radiofusora_schema);
module.exports = Radiofusora;