let Radiofusora = require('./radiofusora.model');
let moment = require('moment');
let _ = require('lodash');

exports.primeraRadiofusora = async (req, res) => {
    let primeraRadiofusoras = await Radiofusora.find().skip(req.body.page)
        .limit(1)
    let listaRadiofusoras = []
    for (let radiofusora of primeraRadiofusoras) {
        const radiofusoraResultados = await buscarTraficosRadiofusora(radiofusora.RADIOBASE)
        listaRadiofusoras.push(radiofusoraResultados)
    }
    let generarListaFechas = await buscarfechasRadiofusora(primeraRadiofusoras[0].RADIOBASE)
    let fechas = obtenerFechas(generarListaFechas[0].fecha, generarListaFechas[generarListaFechas.length - 1].fecha)
    res.json({
        datosRadiofusoras: listaRadiofusoras,
        listaFechas: fechas
    })
}

async function buscarTraficosRadiofusora(radiobase) {
    let radiofusoras = await Radiofusora.find({
        RADIOBASE: radiobase
    })
    radiofusoras = _.sortBy(radiofusoras, 'FECHA')
    let listaFechas = obtenerFechas(radiofusoras[0].FECHA, radiofusoras[radiofusoras.length - 1].FECHA)
    let datosRadiofusoraFecha = []
    datosRadiofusoraFecha.push(radiobase)
    listaFechas.forEach(fecha => {
        fecha = fecha.replace('/', '-').replace('/', '-')
        let fechaAgregada = false
        radiofusoras.forEach(radiofusora => {
            if (fecha == radiofusora.FECHA) {
                datosRadiofusoraFecha.push(
                    Number(radiofusora.TRAFICO)
                )
                fechaAgregada = true
            }
        });
        if (!fechaAgregada)
            datosRadiofusoraFecha.push('')
    });
    return datosRadiofusoraFecha
}

async function buscarfechasRadiofusora(radiobase) {
    let radiofusoras = await Radiofusora.find({
        RADIOBASE: radiobase
    })
    radiofusoras = _.sortBy(radiofusoras, 'FECHA')
    let listaFechas = obtenerFechas(radiofusoras[0].FECHA, radiofusoras[radiofusoras.length - 1].FECHA)
    let datosRadiofusoraFecha = []
    listaFechas.forEach(fecha => {
        fecha = fecha.replace('/', '-').replace('/', '-')
        radiofusoras.forEach(radiofusora => {
            if (fecha == radiofusora.FECHA) {
                datosRadiofusoraFecha.push({
                    fecha: fecha
                })
            }
        });
    });
    return datosRadiofusoraFecha
}

exports.buscarRadiofusora = async (req, res) => {
    let primeraRadiofusoras = await Radiofusora.find({
        RADIOBASE: req.body.radiofusora
    })
    if (!primeraRadiofusoras[0])
        res.json({
            error: "No se encontraron radiofusoras"
        })
    else {
        let listaRadiofusoras = []
        const radiofusoraResultados = await buscarTraficosRadiofusora(req.body.radiofusora)
        listaRadiofusoras.push(radiofusoraResultados)
        let generarListaFechas = await buscarfechasRadiofusora(primeraRadiofusoras[0].RADIOBASE)
        let fechas = obtenerFechas(generarListaFechas[0].fecha, generarListaFechas[generarListaFechas.length - 1].fecha)
        res.json({
            datosRadiofusoras: listaRadiofusoras,
            listaFechas: fechas
        })
    }
}

function obtenerFechas(fechainicio, fechaFinal) {
    fechainicio = new Date(2019,07,23)
    fechaFinal = new Date(2019,08,22)
    var dates = [],
        fechaActual = fechainicio,
        addDays = function (days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        };
    while (fechaActual <= fechaFinal) {
        fechaActual = moment(fechaActual).format('YYYY/MM/DD');
        dates.push(fechaActual);
        fechaActual = addDays.call(fechaActual, 1);
    }
    return dates;
};