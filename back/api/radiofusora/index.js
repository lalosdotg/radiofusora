var router = require('express').Router();
var controller = require('./radiofusora.controller');


router.route('/')
    .post(controller.buscarRadiofusora)

router.route('/primeraRadiofusora')
    .post(controller.primeraRadiofusora)

module.exports = router;