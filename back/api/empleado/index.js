var router=require('express').Router();
var controller=require('./empleado.controller');

router.route('/')
.get(controller.verempleados)
.post(controller.agregar)


router.route('/:id')
.get(controller.verempleado)
.put(controller.modificar)


router.route('/obtener/nombres')
.get(controller.verNombres);
router.get('/habilidad/:habilidad',controller.verempleadosXhabilidad);

module.exports=router;