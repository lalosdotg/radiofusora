let mongoose = require('mongoose');
var Schema = mongoose.Schema;

var proveedor_schema = new Schema({
    nombre: String,
    telefono: String,
    email: String,
    puesto: String,
    domicilio: String,
    fecha: String,
    habilidades : []
});

var empleado = mongoose.model('empleado', proveedor_schema);

module.exports = empleado;