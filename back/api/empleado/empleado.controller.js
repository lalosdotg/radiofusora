var empleado = require('./empleado.model');
var Habilidad = require('../habilidad/habilidad.model');

exports.agregar = (req, res) => {
    empleado.create(req.body)
        .then((empleado) => {
            res.json(empleado)
        })
}

exports.verNombres = (req, res) => {
    empleado.find().distinct('nombre')
        .then((nombres) => {
            res.json(nombres)
        })
}

exports.verempleado = (req, res) => {
    empleado.findById(req.params.id)
        .populate({
            path: 'habilidad',
            model: 'Habilidad'
        })
        .then((empleado) => {

            res.json(empleado)
        })
        .catch((err) => {
            res.status(500).json(err);
        })
}

exports.verempleados = (req, res) => {
    empleado.find()
        .sort({
            nombre: 1
        })
        .then((empleados) => {
            res.json(empleados)
        })
        .catch((err) => {
            res.status(500).json(err);
        })

}

exports.modificar = (req, res) => {
    empleado.findByIdAndUpdate(req.params.id, req.body)
        .then(() => {
            res.json({
                sucess: true
            })
        })
}

exports.verempleadosXhabilidad = (req, res) => {
    var dato = {
        empleados: []
    }
    empleado.find()
        .then((empleados) => {
            empleados.forEach(function (empleado) {

                if (empleado.habilidades.indexOf(req.params.habilidad) !== -1) {
                    dato.empleados.push(empleado)

                }
            });
            return {
                dato
            }
        })
        .then((dato) => {
            res.json(dato)
        })

}