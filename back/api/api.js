var router = require('express').Router();

router.use('/radiofusora', require('./radiofusora'));
router.use('/empleado', require('./empleado'));
router.use('/habilidad', require('./habilidad'))

module.exports = router;