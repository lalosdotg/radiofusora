let express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let path = require('path');
let app = express();
let cors = require('cors')
let env = require('./env')


//TODO: CHANGE FOR LOCALHOST
mongoose
    .set("useCreateIndex", true)
    .connect(env.DBURI, {
        useUnifiedTopology: true,
        useNewUrlParser: true
    })
    .then(() => console.log("BD :)"))
    .catch(err => {
        console.log("Error de conexion")
        console.log(err)
    });


app.use(bodyParser.json({
    limit: '200mb',
    extended: true
}))
app.use(bodyParser.urlencoded({
    limit: '200mb',
    extended: true
}))

app.use(cors({
    credentials: true,
    origin: true
}))
app.options('*', cors({
    credentials: true,
    origin: true
}));

app.use(express.static(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, '/api/public')));
//TODO: COMMENT FOR LOCALHOST
app.use(express.static(path.join(__dirname, './dist')));

app.use('/api', require('./api/api'));
app.use('/', require('./api/'));

app.listen(process.env.PORT || env.PORT, function () {
    console.log("Corriendo en el puerto: " + env.PORT)
})