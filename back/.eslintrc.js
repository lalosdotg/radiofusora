module.exports = {
    root: true,
    env: {
      node: true,
      jest: true,
    },
    extends: "eslint:recommended",
    parserOptions: {
        "ecmaVersion": 11,
        "sourceType": "module"
    },
    rules: {
        'no-var': 1,
        'max-len': [1, { 'code': 80 }],
        'no-whitespace-before-property': [2],
        'no-trailing-spaces': [2],
        'object-curly-spacing': [2, 'always'],
        'space-in-parens': [2, 'never'],
        'space-before-function-paren': [2, {
          'anonymous': 'always',
          'named': 'never',
          'asyncArrow': 'always'
        }],
        'no-confusing-arrow': [2],
        'prefer-arrow-callback': [2],
        'keyword-spacing': [2],
        'no-multi-spaces': [2],
        'no-await-in-loop': [2],
        'function-paren-newline': [2, 'multiline-arguments'],
        'no-console': [1],
        'no-magic-numbers': [1],
        'vars-on-top': [1]
    }
};
